## Prometheus_Slurm_Exporter
- Prometheus exporter for HPC metrics extracted from the squeue.
- The system monitor job queues for the HPC scheduler SLURM, and is build around Prometheus for data collection and time series storage and Grafana for visualization.
- The project goal is creating a monitoring system to capture real-time information regarding the number of jobs running on HPC clusters, while integrating the work with DevOps and Continuous Integration practices and tools. The information regarding the status of the job queues will be collected, processed and stored as time series and summarized and made available to users and administrators in the form of graphs.

## Features
- Data collection and time series storage using prometheus 
- Data visualization on prometheus and grafana dashboards
## Requirements
- python3
- Prometheus_client, python version
## Supported metric types
Gauge
## Status of the slurm_job
- PENDING: Jobs awaiting for resource allocation.
- RUNNING: Jobs currently allocated.
- SUSPENDED: Job has an allocation but execution has been suspended.
- CANCELLED: Jobs which were explicitly cancelled by the user or system administrator.
- COMPLETING: Jobs which are in the process of being completed.
- COMPLETED: Jobs have terminated all processes on all node with an exit code of zero.
- CONFIGURING: Jobs have been allocated resources, but are waiting for them to become ready for use.
- FAILED: Jobs terminated with a non-zero exit code or other failure condition.
- TIMEOUT: Jobs terminated upon reaching their time limit.
- PRE_EMPTED: Jobs terminated due to preemption.
- NODE_FAIL: Jobs terminated due to failure of one or more allocated nodes.


## Usage
- $ git clone https://gitlab.com/surfprace/joemah
- $ cd ../joemah/
- ../joemah$ source venv/bin/activate
- $ python3 prom_exporter.py [-h] 


## License
- GPLv3 https://www.gnu.org/licenses/gpl-3.0.html




