#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
* This script allows to acts as an exporter for HPC 
  jobs with the help of the prometheus_client library.
* This file is run as a python scricpt on command line / terminal.
* It employs the use of  `argparse` Python module to support command line arguments 
  and make possible for the exporter to take the correct 
  command to execute to get the SLURM statistics.

It contains the following functions:
--------
** argparser: 
        allows the user to  parse required and optional 
        commands on terminal when executing this file.
        It makes the subprocess to execute an 
        arbitrary command that is passed as parameter to this (prom_exporter.py) script.
--------
** export:
       for  processing the summary dictionary to handle it to Prometheus:
       gives summary metrics of slurm jobs for each slurm_(project/group)
--------
** main:
      





"""

import time
import argparse

from prometheus_client import start_http_server, Gauge
from prometheus_exporter.slurm_parser import get_command_output, parser

# sleep after 10s
elapse = 10 
cmd_default = "squeue" # the deafault slurm project generator
summary_metrics = Gauge('group_jobs', 
                        'state information',
                        ['status', 'slurm_project'])


def argparser():

    """
    /*  * Allows the user to parse argments on command line. 
        * Makes it possible for the subprocess to execute scripts parsed via the command line. 
        * Give the user indications on how to execute the program
        --------
        Returns:
            parser.parse_args()
                - a list of argments given by the user via command line
    */
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cmd',
                        action='store',
                        dest='squeue_info',
                        type=str,
                        nargs='?',
                        default=cmd_default,
                        help='command to be executed by the exporter. \
                              This command will provide the SLURM job info \
                              used by the exporter. \
                              Default: "squeue"')
    parser.add_argument('-p', '--port',
                        action='store',
                        dest='port',
                        type=int,
                        default=8000,
                        help='Port where the exporter will be running. \
                              Default: TCP 8000')

    return parser.parse_args()


def export(dictt):
    """
    /* 
       Takes as input a dict of dicts containing slurm projects and their states.
       For each project, asign it to the slurm_project
       And for each project, take the state and asign it to  the status.
       Process the summary dictionary to handle it to Prometheus.
       ----
       Parameters:
          dictt: dictionary of slurm(projects/groups) dictionaries
    */
    """
    for key in dictt:
        project_name = key
        group_jobs = dictt[key]
        for state in group_jobs:
            summary_metrics.labels(
                status=state,
                slurm_project=project_name
            ).set(group_jobs[state])


def main():
    """ """
    args = argparser()
    cmd = args.squeue_info
    port = args.port

    # start up the server to expose the metrics
    start_http_server(port)
    while True:
        # get the lines from  the given squeue
        lines = get_command_output(cmd)
        # get the dictionary from  the given lines
        dictt = parser(lines)
        # export the metrics
        export(dictt)
        # wait
        time.sleep(elapse)


if __name__ == "__main__":
    main()
