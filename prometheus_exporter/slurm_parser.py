#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The parser script analyze SLURM squeue output and generate 
a summary of the jobs on the queue grouped by project and state.
Receive as input parameter a list of strings, where each string is a 
line with a job record
It return a dictionary of dictionaries, where each key presents an (account/project), 
and it has associated a dictionary with a summary of the number of 
jobs in each different state (RUNNING, CANCELLED, ...NODE_FAIL) for the given (group/project).

This script allows the user to  parse the squeue jobs as a dictionary of slurm_(group/project)

This file is imported as a module to the prometheus exporter and contain 
the following functions:

** get_commmand_output: returns a list of strings (lines)
 * parser: returns a dictionary of slurm_(project/groups) dictionaries 
"""


import subprocess


# get the processes from  an squeue.py
def get_command_output(cmd):
    """
    Gets the command output and 
    Returns the process as a list of (string) lines.
    Parameters:
    * cmd: str (required) 
         eg cmd = 'python3 squeue.py'
         the default is squeue
    Returns:
    * process:
        a list of strings
    """
    process = subprocess.run(cmd.split(),
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    stdout = process.stdout 
    process = stdout.splitlines()
    return process


def parser(lines):
    """ 
    This function returns, a dictionary  of dictionaries of slurm_(group/project) 

    Parameters:
    -----------
    ** lines: list
        a list of strings

    Returns:
    -------
        project_dict: dict
            a dictionary  of dictionaries each  containing a slurm project/group  
    """
    project_dict = dict() # dictionary of dicts per project/group
    temp_lines = [string for string in lines if string != ""] # check  if the lines contain empty strings

    for line in temp_lines:
        array = line.strip().split(',')

        # make sure that the array has a minimum length
        if len(array) >= 12 and array[2] not in project_dict:
            default_states = {
                'PENDING': 0,
                'RUNNING': 0,
                'SUSPENDED': 0,
                'CANCELLED': 0,
                'COMPLETING': 0,
                'COMPLETED': 0,
                'CONFIGURING': 0,
                'FAILED': 0,
                'TIME_OUT': 0,
                'PRE_EMPTED': 0,
                'NODE_FAIL': 0
            }
            # append the initial states to  each group/project
            project_dict[array[2]] = default_states

        if len(array) >= 12 and array[2] in project_dict:
            project_dict[array[2]][array[9]] += 1 # increment the state/status

    # return the dictionary obtained
    return project_dict
