#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages

setup(
    name="prometheus_exporter",
    version="0.1.1",
    author="JOEMAH CATHAL JUAN",
    author_email='joemahmagenya@gmail.com',
    url='https://gitlab.com/surfprace/joemah',
    include_package_data=True,
    test_suite="tests",
    zip_safe=False,
    description="Python exporter for Prometheus monitoring system",
    # long_description = long_description,
    long_description_content_type='text/markdown',
    keywords="prometheus monitoring instrumentation  exporter",
    entry_points={
        'console_scripts': [
            'prom_exporter = prometheus_exporter.prom_exporter:main',
        ]
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    packages=find_packages(),
    install_requires=['argparse', 'prometheus_client'],
    licence="GPLv3",
)
